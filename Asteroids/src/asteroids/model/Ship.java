package asteroids.model;

import asteroids.control.IShip;
import be.kuleuven.cs.som.annotate.*;
import java.lang.Math;

/**
 * A class for dealing with spaceships, containing all methods affecting their parameters,
 * such as velocity, position, orientation and radius. It also deals with possible actions for the 
 * spaceship and makes easy calculations concerning an eventual collision between two spaceships
 * possible.
 * 
 * @invar	The velocity of each spaceship must be a valid velocity for a spaceship.
 * 			| isValidVelocity(xVelocity, yVelocity)
 * @invar 	The direction of each spaceship must be a valid direction for a spaceship.
 * 			| isValidDirection(direction)
 * @invar	The radius of each spaceship must be a valid radius for a spaceship.
 * 			| isValidRadius(radius)
 * 
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 2.0 (post 1.3.6)
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-1
 */

public class Ship implements IShip {
	
	/**
	 * Initialise the new spaceship with a given horizontal and vertical position,
	 * horizontal and vertical velocity, direction and radius.
	 * 
	 * @param 	xPosition
	 * 			The horizontal position of the new spaceship.
	 * @param 	yPosition
	 * 			The vertical position of the new spaceship.
	 * @param 	xVelocity
	 * 			The horizontal velocity of the new spaceship.
	 * @param	yVelocity
	 * 			The vertical velocity of the new spaceship.
	 * @param 	direction
	 * 			The direction of the new spaceship.
	 * @param 	radius
	 * 			The radius of the new spaceship.
	 * @pre		The given direction must be a valid direction for the spaceship.
	 * 		  	| isValidDirection(direction)
	 * @post	The horizontal position of the new spaceship is equal to the given horizontal position.
	 * 		  	| (new this).getXPosition() == xPosition
	 * @post	The vertical position of the new spaceship is equal to the given vertical position.
	 * 		  	| (new this).getYPosition() == yPosition
	 * @post	The horizontal velocity of the new spaceship is equal to the given horizontal velocity.
	 * 		  	| (new this).getXVelocity() == xVelocity
	 * @post	The vertical velocity of the new spaceship is equal to the given vertical velocity.
	 * 		  	| (new this).getYVelocity() == yVelocity
	 * @post	The direction of the new spaceship is equal to the given direction.
	 * 		  	| (new this).getDirection() == direction
	 * @post    The radius of the new spaceship is equal to the given radius.
	 * 		  	| (new this).getRadius() == radius
	 * @throws 	IllegalArgumentException
	 * 			The given radius is not a valid radius for the given spaceship.
	 * 		  	| !isValidRadius(radius)
	 */
	public Ship(double xPosition, double yPosition, double xVelocity, double yVelocity, 
			double direction, double radius) throws IllegalArgumentException {
			setXPosition(xPosition);
			setYPosition(yPosition);
			double[] velocity = adaptVelocity(xVelocity,yVelocity);
			setXVelocity(velocity[0]);
			setYVelocity(velocity[1]);
			setDirection(direction);
		if (!isValidRadius(radius))
			throw new IllegalArgumentException();
		this.radius = radius;
	}
	
	/**
	 * Return the horizontal position of the spaceship, expressed in kilometers.
	 * DEFENSIVE
	 * 
	 * @return 	The horizontal position of the spaceship. 
	 * 			| this.xPosition
	 */
	@Basic
	public double getXPosition(){
		return this.xPosition;
	}
	
	/**
	 * Set the horizontal position of the spaceship, expressed in kilometers. 
	 * DEFENSIVE
	 * 
	 * @param 	xPosition
	 * 			The horizontal position of the spaceship.
	 * @post	The new horizontal position of the spaceship becomes the given horizontal position.
	 * 			| (new this).getXPosition() == xPosition
	 * @throws 	IllegalArgumentException
	 * 			Will throw an exception if the entry is not a number.
	 * 			| Double.isNaN(xPosition)
	 * @note	@Model tag used because this private method is used in the specification of move
	 */
	@Model
	private void setXPosition(double xPosition) throws IllegalArgumentException {
		if(Double.isNaN(xPosition))
			throw new IllegalArgumentException();
		this.xPosition = xPosition;
	}
	
	/**
	 * Variable storing the horizontal position of the spaceship.
	 */
	private double xPosition;

	/**
	 * Return the vertical position of the spaceship, expressed in kilometers.
	 * DEFENSIVE
	 * 
	 * @return 	The vertical position of the spaceship.
	 * 			| this.yPosition
	 */
	@Basic
	public double getYPosition(){
		return this.yPosition;
	}
	
	/**
	 * Set the vertical position of the spaceship, expressed in kilometers. 
	 * DEFENSIVE
	 * 
	 * @param 	yPosition
	 * 			The vertical position of the spaceship.
	 * @post	The new vertical position of the spaceship becomes the given vertical position.
	 * 			| (new this).getYPosition() == yPosition
	 * @throws 	IllegalArgumentException
	 * 			Will throw an exception if the entry is not a number.
	 * 			| Double.isNaN(yPosition)
	 * @note	@Model tag used because this private method is used in the specification of move
	 */
	@Model
	private void setYPosition(double yPosition) throws IllegalArgumentException {
		if(Double.isNaN(yPosition))
			throw new IllegalArgumentException();
		this.yPosition = yPosition;
	}
	
	/**
	 * Variable storing the vertical position of the spaceship.
	 */
	private double yPosition;
	
	/**
	 * Return the horizontal velocity of the spaceship, expressed in kilometers/second.
	 * TOTAL
	 * 
	 * @return 	The horizontal velocity of the spaceship.
	 * 			| this.xVelocity
	 * @note	@Raw because we don't know if the prime object already satisfies 
	 * 			all its class invariants.
	 */
	@Basic @Raw
	public double getXVelocity(){
		return this.xVelocity;
	}
	
	/**
	 * Return the vertical velocity of the spaceship, expressed in kilometers/second.
	 * TOTAL
	 * 
	 * @return 	The vertical velocity of the spaceship.
	 * 			| this.yVelocity
	 */
	@Basic @Raw
	public double getYVelocity(){
		return this.yVelocity;
	}
	
	/**
	 * Checks whether the given velocity doesn't exceed the speed limit.
	 * TOTAL
	 * 
	 * @param 	xVelocity
	 * 			The horizontal velocity that has to be checked.
	 * @param	yVelocity
	 * 			The vertical velocity that has to be checked.
	 * @return	False if the total velocity exceeds the speed limit.
	 * 			| result == (Math.hypot(xVelocity,yVelocity) < getSpeedLimit())
	 */
	public static boolean isValidVelocity(double xVelocity, double yVelocity){  
		return (Math.hypot(xVelocity,yVelocity) <= getSpeedLimit());
	}
	
	/**
	 * Adapt the horizontal and vertical velocities if the resulting total velocity 
	 * exceeds the speed limit.
	 * TOTAL
	 * 
	 * @param 	xVelocity
	 * 			The horizontal velocity that has to be adapted.
	 * @param 	yVelocity
	 * 			The vertical velocity that has to be adapted.
	 * @post	The new horizontal velocity of the spaceship must be a valid number.
	 * 			| if (Double.isNaN(xVelocity))
	 * 			|   then xVelocity == 0
	 * @post	The new vertical velocity of the spaceship must be a valid number.
	 * 			| if (Double.isNaN(yVelocity))
	 * 			|   then yVelocity == 0
	 * @return	An array representing the adapted horizontal and vertical velocities.
	 * 			| if(!isValidVelocity(xVelocity,yVelocity))
	 * 			|	then ((result[0] == xVelocity*(getSpeedLimit()/Math.hypot(xVelocity, yVelocity)))
	 * 			|	&&	  (result[1] == yVelocity*(getSpeedLimit()/Math.hypot(xVelocity, yVelocity)))
	 * 			| else ((result[0] == xVelocity)	&& 	(result[1] == yVelocity))
	 */
	@Raw
	public double[] adaptVelocity(double xVelocity, double yVelocity) {
		double[] velocity = new double[2];
		if(Double.isNaN(xVelocity))
			xVelocity = 0;
		if(Double.isNaN(yVelocity))
			yVelocity = 0;
		if(!isValidVelocity(xVelocity,yVelocity)){
				double proportion = getSpeedLimit()/Math.hypot(xVelocity, yVelocity);
				velocity[0] = xVelocity * proportion;
				velocity[1] = yVelocity * proportion;
		} else {
			velocity[0] = xVelocity;
			velocity[1] = yVelocity;
		}
		return velocity;
	}
	
	
	/**
	 * Set the horizontal velocity of the spaceship to the given entry.
	 * TOTAL
	 * 
	 * @param 	xVelocity
	 * 			The new horizontal velocity of the spaceship.
	 * @post	The new horizontal velocity of the spaceship becomes the given horizontal velocity.
	 * 			| (new this).getXVelocity() == xVelocity
	 * @note	This setter has been made public as we need to use it in the implementation
	 * 			of test methods in ShipTest.
	 */
	@Raw
	public void setXVelocity(double xVelocity){				
		this.xVelocity = xVelocity;
	}
	
	/**
	 * Set the vertical velocity of the spaceship to the given entry.
	 * TOTAL
	 * 
	 * @param 	yVelocity
	 * 			The new vertical velocity of the spaceship.
	 * @post	The new vertical velocity of the spaceship becomes the given vertical velocity.
	 * 			| (new this).getYVelocity() == yVelocity
	 * @note	This setter has been made public as we need to use it in the implementation
	 * 			of test methods in ShipTest.
	 */
	@Raw
	public void setYVelocity(double yVelocity){	
		this.yVelocity = yVelocity;
	}
	
	/**
	 * Variable storing the horizontal velocity of the spaceship.
	 */
	private double xVelocity;
	
	/**
	 * Variable storing the vertical velocity of the spaceship.
	 */
	private double yVelocity;
	
	/**
	 * Return the speedlimit of the spaceship.
	 * 
	 * @return	The speedlimit of the spaceship.
	 * 			| lightSpeed   						//later: this.speedLimit
	 * @note	At a later stage, the speedlimit will be a parameter of the spaceship as well
	 * 			and it won't be compulsorily equal to the speed of light anymore.
	 * 			The static property of the method will have to disappear in this method and 
	 * 			in the method isValidVelocity, and in the class ShipTest as well.
	 * @note	This is an immutable getter because it will always return the same speed limit 
	 * 			after being initialised once.
	 */
	@Basic @Immutable
	public static double getSpeedLimit() {
		return lightSpeed;	 
	}
	
	/**
	 * Variable storing the speed of the light.
	 */
	private static final double lightSpeed = 300000;
	
	/**
	 * Return the direction of the spaceship, expressed in radians.
	 * NOMINAL
	 * 	
	 * @return 	The direction of the spaceship
	 * 			| this.direction
	 */
	@Basic @Raw
	public double getDirection(){
		return this.direction;
	}
	
	/**
	 * Set the direction of the spaceship to the given entry.
	 * 
	 * @param 	direction
	 * 			The new direction of the spaceship
	 * @pre		The direction of the spaceship must be a valid direction.
	 * 			| isValidDirection(direction)
	 * @post	The direction of the spaceship is equal to the given direction.
	 * 			| (new this).getDirection() == direction
	 */
	@Model @Raw
	private void setDirection(double direction){	
		assert isValidDirection(direction);				
		this.direction = direction;
	}
	
	/**
	 * Checks if the given entry is a valid direction for the spaceship. 
	 * 
	 * @param 	direction
	 * 			The direction that has to be checked.
	 * @pre		The given direction must be a valid number (and nothing else).
	 * 			| !Double.isNaN(direction)
	 */
	public static boolean isValidDirection(double direction){	
		return !Double.isNaN(direction); 						
	}
	
	/**
	 * Variable storing the direction of the spaceship.
	 */
	private double direction;
	
	/**
	 * Return the radius of the spaceship, expressed in kilometers.
	 * DEFENSIVE
	 * 
	 * @return 	The radius of the spaceship
	 * 			| this.radius
	 */
	@Basic @Raw
	public double getRadius(){
		return this.radius;
	}
	
	/**
	 * Check if the radius is a valid radius for the spaceship.
	 * 
	 * @param 	radius
	 * 			The radius that needs to be checked.
	 * @return	True if and only if the radius is a number and if the radius is larger than 10.
	 * 			| result == (radius > 10) && !Double.isNaN(radius)
	 */
	public static boolean isValidRadius(double radius){
		return (radius > 10) && !Double.isNaN(radius);
	}
	
	/**
	 * Variable storing the radius of the spaceship.
	 */
	private final double radius;
	
	/**
	 * Moves the spaceship around for a given duration.
	 * DEFENSIVE
	 * 
	 * @param 	duration
	 * 			The duration of the motion of the spaceship.
	 * @effect	The new horizontal position of the spaceship is equal to its actual position 
	 * 			increased by an amount equal to its horizontal velocity times the duration of the motion.
	 * 			| this.setXPosition(getXPosition() + getXVelocity()*duration)
	 * @effect	The new vertical position of the spaceship is equal to its actual position 
	 * 			increased by an amount equal to its vertical velocity times the duration of the motion.
	 * 			| this.setYPosition(getYPosition() + getYVelocity()*duration)
	 * @throws	IllegalArgumentException
	 * 			Will throw an exception if the duration is a valid duration.
	 * 			| (!isValidDuration(duration))
	 */
	public void move(double duration) throws IllegalArgumentException{
		if (!isValidDuration(duration))
			throw new IllegalArgumentException();
		this.setXPosition(getXPosition() + getXVelocity()*duration);
		this.setYPosition(getYPosition() + getYVelocity()*duration);
	}
		
	/**
	 * Check if the given duration is a valid duration.
	 * 
	 * @param 	duration
	 * 			The duration that has to be checked.
	 * @return 	True and only true if the duration is a positive number.
	 * 			| result == (duration >= 0) && (!Double.isNaN(duration))
	 */
	public static boolean isValidDuration(double duration){
		return (duration >= 0) && (!Double.isNaN(duration));
	}
	
	/**
	 * Rotates the direction of the spaceship over a given angle.
	 * NOMINAL
	 * 
	 * @param 	angle
	 * 			The angle over which the spaceship rotates.
	 * @pre		The new direction of the spaceship must be a valid direction.
	 * 			| isValidDirection(getDirection() + angle)
	 * @effect	The new direction of the spaceship is equal to its actual direction increased
	 * 			by the given angle.
	 *			| this.setDirection(getDirection() + angle)
	 * @note	@Raw because the new angle may violate the class invariant during the method.
	 */
	@Raw
	public void turn(double angle) {
		assert isValidDirection(getDirection() + angle);
		this.setDirection(getDirection() + angle);
	}
	
	/**
	 * Accelerates the spaceship in the spaceship's direction by a given amount.
	 * TOTAL
	 * 
	 * @param 	acceleration
	 * 			The acceleration of the spaceship.
	 * @effect	The velocity is increased by the given acceleration after being adapted.
	 * 			| setXVelocity(adaptVelocity(getXVelocity() + acceleration*Math.cos(this.getDirection())),getYVelocity() + acceleration*Math.sin(this.getDirection())[0])
	 * 			| setYVelocity(adaptVelocity(getYVelocity() + acceleration*Math.cos(this.getDirection())),getYVelocity() + acceleration*Math.sin(this.getDirection())[1])
	 * @effect	The velocity does not change if the given acceleration is not a positive number.
	 * 			| if((Double.isNaN(acceleration) || (acceleration < 0)))
	 * 			| 	then ((setXVelocity(getXVelocity()))
	 * 			|	&&	  (setYVelocity(getYVelocity())))
	 */
	public void thrust(double acceleration){
		if((Double.isNaN(acceleration) || (acceleration < 0)))
			acceleration = 0;
		double newXVelocity = getXVelocity() + acceleration*Math.cos(this.getDirection());
		double newYVelocity = getYVelocity() + acceleration*Math.sin(this.getDirection());
		double[] velocity = adaptVelocity(newXVelocity, newYVelocity);
		this.setXVelocity(velocity[0]);
		this.setYVelocity(velocity[1]);
	}

	/**
	 * Returns the distance between this spaceship and another spaceship. The distance is expressed
	 * in kilometers and represents the distance between the circle boundaries.
	 * DEFENSIVE
	 * 
	 * @param 	ship
	 * 			The concurrent ship
	 * @return	The distance between the current ship and the concurrent ship.
	 * 			| result == (Math.hypot(this.getXPosition()-ship.getXPosition(),this.getYPosition()-ship.getYPosition()) - Math.abs(this.getRadius() + ship.getRadius()))
	 * @throws	IllegalArgumentException
	 * 			Will throw an exception if the entry is not a ship.
	 * 			| (ship == null)
	 * @note	Hypot returns the square root out of (x�+y�) without intermediate overflow of underflow!
	 * @note	Cast used to convert ship from the type IShip to the type Ship.
	 */
	public double getDistanceBetween(IShip ship) throws IllegalArgumentException {  
		if (ship == null)
			throw new IllegalArgumentException();
		Ship castedShip = (Ship) ship;
		double distance = Math.hypot(this.getXPosition()-castedShip.getXPosition(),this.getYPosition()-castedShip.getYPosition());
			return (distance - (Math.abs(this.getRadius() + castedShip.getRadius())));
	}
	
	/**
	 * Checks whether the spaceship overlaps with the given spaceship.
	 * DEFENSIVE
	 * 
	 * @param 	ship
	 * 			The concurrent ship that has to be checked for overlapping
	 * @return	True if and only if the spaceships overlap.
	 * 			| result == (this.getDistanceBetween(ship) < 0)
	 * @throws	IllegalArgumentException
	 * 			Will throw an exception if the entry is not a ship.
	 * 			| (ship == null)
	 * @note	A ship always overlaps with itself.
	 */
	public boolean overlap(IShip ship) throws IllegalArgumentException {
		if (ship == null)
			throw new IllegalArgumentException();
		return (this.getDistanceBetween(ship) < 0);
	}
	
	/**
	 * Returns the time until the spaceship and the given spaceship will collide.
	 * The time will be expressed in seconds. 
	 * DEFENSIVE
	 * 
	 * @param 	ship
	 * 			The concurrent ship
	 * @return	The time until the ships collide.
	 * 			| let dotProduct = (this.getXPosition() - ship.getXPosition())*(this.getXVelocity() - ship.getXVelocity()) + (this.getYPosition() - ship.getYPosition())*(this.getYVelocity() - ship.getYVelocity())
	 * 			| let velocityNorm = sqrt((this.getXVelocity() - ship.getXVelocity())�+(this.getYVelocity() - ship.getYVelocity())�)
	 * 			| let positionNorm = sqrt((this.getXPosition() - ship.getXPosition())�+(this.getYPosition() - ship.getYPosition())�)
	 * 			| let d = dotProduct�-velocityNorm�*(positionNorm�-(this.getDistanceBetween(ship))�)
	 * 			| result == (-(dotProduct + sqrt(d))/velocityNorm�)
	 * @return	The time is infinite if the ships will never collide.
	 * 			| result == Double.POSITIVE_INFINITY 
	 * @throws	IllegalArgumentException
	 * 			Will throw an exception if the entry is not a ship.
	 * 			| (ship == null)
	 * @note	Cast used to convert ship from the type IShip to the type Ship.
	 * @note	(This is a method consisting of a lot of mathematical operations, and we are not sure
	 * 			about the way of writing our formal specification of the result. Any comment to help 
	 * 			us make it better and shorter is welcome.)
	 */
	public double getTimeToCollision(IShip ship) throws IllegalArgumentException {
		if (ship == null)
			throw new IllegalArgumentException();
		Ship castedShip = (Ship) ship;
		double deltaXVelocity = this.getXVelocity() - castedShip.getXVelocity();
		double deltaYVelocity = this.getYVelocity() - castedShip.getYVelocity();
		double deltaXPosition = this.getXPosition() - castedShip.getXPosition(); 
		double deltaYPosition = this.getYPosition() - castedShip.getYPosition(); 
		double velocityNorm = Math.hypot(deltaXVelocity,deltaYVelocity);
		double positionNorm = Math.hypot(deltaXPosition,deltaYPosition);
		double dotProduct = deltaXVelocity*deltaXPosition + deltaYVelocity*deltaYPosition;
		double d = Math.pow(dotProduct,2)-Math.pow(velocityNorm,2)*(Math.pow(positionNorm, 2)-Math.pow(getDistanceBetween(ship), 2));
		if (dotProduct >= 0)
			return Double.POSITIVE_INFINITY;
		if (d <= 0)
			return Double.POSITIVE_INFINITY;
		if (-(dotProduct + Math.sqrt(d))/(Math.pow(velocityNorm,2)) < 0)			
			return Double.POSITIVE_INFINITY;
		else
			return -(dotProduct + Math.sqrt(d))/(Math.pow(velocityNorm,2));
	}
	
	/**
	 * Returns the collision position of the spaceship and its concurrent.
	 * DEFENSIVE
	 * 
	 * @param 	ship
	 * 			The concurrent ship
	 * @return	An array representing the collision position. This array is obtained by creating clones
	 * 			of the two existing ships, by making them collide and by calculating the exact
	 * 			colliding point. 
	 * 			| let shipCloneOne = this.clone()
	 * 			| let shipCloneTwo = ship.clone()
	 * 			| shipCloneOne.move(this.getTimeToCollision(ship))
	 * 			| shipCloneTwo.move(this.getTimeToCollision(ship))
	 * 			| result == ((vector[0] == shipCloneOne.getXPosition() 
	 * 			|	+ (shipCloneOne.getXPosition() - shipCloneTwo.getXPosition())
	 * 			|	* shipCloneOne.getRadius()/(shipCloneOne.getRadius()+shipCloneTwo.getRadius()))
	 * 			|	&& (vector[1] == shipCloneOne.getYPosition()
	 * 			|	+ shipCloneOne.getYPosition() - shipCloneTwo.getYPosition()
	 * 			|	* shipCloneOne.getRadius()/(shipCloneOne.getRadius()+shipCloneTwo.getRadius())))
	 * @throws	IllegalArgumentException
	 * 			Will throw an exception if the entry is not a ship.
	 * 			| (ship == null)
	 * @note	Cast used to convert ship from the type IShip to the type Ship
	 * @note	Cloning the castedShip is not necessary, as the castedShip is already a kind of clone
	 * 			itself, but it makes the code a lot more easy to understand.
	 * @note	(This is a method consisting of a lot of mathematical operations, and we are not sure
	 * 			about  the way of writing our formal specification of the result. Any comment to help 
	 * 			us make it better and shorter is welcome.)
	 */
	public double[] getCollisionPosition(IShip ship) throws IllegalArgumentException {
		if (ship == null)  					
			throw new IllegalArgumentException();
		Ship castedShip = (Ship) ship;
		if (getTimeToCollision(castedShip) == Double.POSITIVE_INFINITY)
			return null;
		Ship shipCloneOne = this.clone();
		Ship shipCloneTwo = castedShip.clone();			
		shipCloneOne.move(this.getTimeToCollision(castedShip));
		shipCloneTwo.move(this.getTimeToCollision(castedShip));
		double[] vector = new double[2];
		vector[0] = shipCloneOne.getXPosition() - shipCloneTwo.getXPosition();
		vector[1] = shipCloneOne.getYPosition() - shipCloneTwo.getYPosition();
		double proportion = shipCloneOne.getRadius()/(shipCloneOne.getRadius()+shipCloneTwo.getRadius());
		vector[0] = shipCloneOne.getXPosition() + vector[0]*proportion;
		vector[1] = shipCloneOne.getYPosition() + vector[1]*proportion;
		return vector;
	}
	
	/**
	 * Makes a clone of the ship upon which the method is applied.
	 * 
	 * @return 	A new ship with the same properties as the existing ship.
	 * 			| result == new Ship(getXPosition(), getYPosition(), getXVelocity(), getYVelocity(), getDirection(), getRadius())
	 */
	@Override
	protected Ship clone(){
		return new Ship(getXPosition(), getYPosition(), getXVelocity(), getYVelocity(), getDirection(), getRadius());
	}

}
