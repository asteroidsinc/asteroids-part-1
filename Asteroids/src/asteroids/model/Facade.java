package asteroids.model;

import asteroids.control.IFacade;
import asteroids.control.IShip;
import asteroids.exception.ModelException;

/**
 * A class for reducing dependencies of outside code on the inner workings of the Ship class and
 * for making it easier to use.
 * 
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version	2.0 (post 1.3.6)
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-1
 */
public class Facade implements IFacade {

	@Override
	public IShip createShip() throws ModelException {
		try {
			return createShip(0, 0, 0, 0, 10+Double.MIN_VALUE , 0);  
		  } catch (IllegalArgumentException exc) {
			  throw new ModelException(exc);
		  }
	}

	@Override
	public IShip createShip(double x, double y, double xVelocity,
			double yVelocity, double radius, double angle) throws ModelException {
		try {
			  return new Ship(x, y, xVelocity, yVelocity, angle, radius);
		  } catch (IllegalArgumentException exc) {
			  throw new ModelException(exc);
		  }
	}

	@Override
	public double getX(IShip ship) {
		return ship.getXPosition();
	}

	@Override
	public double getY(IShip ship) {
		return ship.getYPosition();
	}

	@Override
	public double getXVelocity(IShip ship) {
		return ship.getXVelocity();
	}

	@Override
	public double getYVelocity(IShip ship) {
		return ship.getYVelocity();
	}

	@Override
	public double getRadius(IShip ship) {
		return ship.getRadius();
	}

	@Override
	public double getDirection(IShip ship) {
		return ship.getDirection();
	}

	@Override
	public void move(IShip ship, double dt) throws ModelException {
		try {
			  ship.move(dt);
		  } catch (IllegalArgumentException exc) {
			  throw new ModelException(exc);
		  }	
	}

	@Override
	public void thrust(IShip ship, double amount) {
		ship.thrust(amount);
	}

	@Override
	public void turn(IShip ship, double angle) {
		ship.turn(angle);
	}

	@Override
	public double getDistanceBetween(IShip ship1, IShip ship2) throws ModelException {
		try {
			  return ship1.getDistanceBetween(ship2); 
		  } catch (IllegalArgumentException exc) {
			  throw new ModelException(exc);
		  }
	}

	@Override
	public boolean overlap(IShip ship1, IShip ship2) throws ModelException {
		try {
			  return ship1.overlap(ship2);
		  } catch (IllegalArgumentException exc) {
			  throw new ModelException(exc);
		  }	
	}

	@Override
	public double getTimeToCollision(IShip ship1, IShip ship2) throws ModelException {
		try {
			  return ship1.getTimeToCollision(ship2);
		  } catch (IllegalArgumentException exc) {
			  throw new ModelException(exc);
		  }
	}

	@Override
	public double[] getCollisionPosition(IShip ship1, IShip ship2) throws ModelException {
		try {
			  return ship1.getCollisionPosition(ship2);
		  } catch (IllegalArgumentException exc) {
			  throw new ModelException(exc);
		  }
	}

}