package asteroids.control;

/**
 * Classes for representing ships should implement <code>IShip</code>. Do not
 * modify this file.
 */
public interface IShip {

	public double getXPosition();

	public double getYPosition();

	public double getXVelocity();

	public double getYVelocity();

	public double getRadius();

	public double getDirection();

	public void move(double dt);

	public void thrust(double amount);

	public void turn(double angle);

	public double getDistanceBetween(IShip ship2);

	public boolean overlap(IShip ship2);

	public double getTimeToCollision(IShip ship2);

	public double[] getCollisionPosition(IShip ship2);

}
