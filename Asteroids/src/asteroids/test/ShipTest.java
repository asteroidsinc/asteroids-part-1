
package asteroids.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import asteroids.model.*;
import asteroids.util.Util;

/**
 * A class for testing the functionalities of the class Ship.
 * 
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 2.0 (post 1.3.6)
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-1
 */
public class ShipTest {

	private Ship	ship0_0_0_10_60_13, 	ship0_0_min15_0_157_50,		ship0_0_0_min10_45_15, 	
					ship0_30_10_0_108_15,	ship0_34_0_min10_135_11,	ship0_30_0_min20_0_15,
					ship20_0_0_10_0_13,		ship40_min10_10_0_0_12;
					
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		ship0_0_0_10_60_13 = new Ship(0,0,0,10,Math.PI/3,13);
		ship0_0_0_min10_45_15 = new Ship(0,0,0,-10,Math.PI/4,15);
		ship0_0_min15_0_157_50 = new Ship(0,0,-15,0,7*Math.PI/8,50);
		ship0_30_0_min20_0_15 = new Ship(0,30,0,-20,0,15);
		ship0_30_10_0_108_15 = new Ship(0,30,10,0,3*Math.PI/5,15);
		ship20_0_0_10_0_13 = new Ship(20,0,0,10,0,13);
		ship0_34_0_min10_135_11 = new Ship(0,34,0,-10,3*Math.PI/4,11);
		ship40_min10_10_0_0_12 = new Ship(40,-10,10,0,0,12);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void Constructor_LegalCase() throws Exception {
		Ship ship = new Ship(0,0,-10*Math.sqrt(2),10*Math.sqrt(2),2*Math.PI/3,25);
		assert Util.fuzzyEquals(0, ship.getXPosition());
		assert Util.fuzzyEquals(0, ship.getYPosition());
		assert Util.fuzzyEquals(-10*Math.sqrt(2), ship.getXVelocity());
		assert Util.fuzzyEquals(10*Math.sqrt(2), ship.getYVelocity());
		assert Util.fuzzyEquals(2*Math.PI/3, ship.getDirection());
		assert Util.fuzzyEquals(25, ship.getRadius());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public final void Constructor_IllegalCaseOutOfBounds() throws Exception {
		new Ship(0,0,-10*Math.sqrt(2),10*Math.sqrt(2),3*Math.PI/4,5);		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public final void Constructor_IllegalCaseNaN() throws Exception {
		new Ship(Double.NaN,0,10,2,Math.PI/2,13);
	}
	
	@Test
	public final void isValidVelocity_TrueCase() {
		assertTrue (Ship.isValidVelocity(857,546));
		assertTrue (Ship.isValidVelocity(0,0));
		assertTrue (Ship.isValidVelocity(-546,302));
	}
	
	@Test
	public final void isValidVelocity_FalseCase() {
		assertFalse (Ship.isValidVelocity(Double.POSITIVE_INFINITY,231));
		assertFalse (Ship.isValidVelocity(72,Double.POSITIVE_INFINITY));
		assertFalse (Ship.isValidVelocity(Double.NEGATIVE_INFINITY,231));
		assertFalse (Ship.isValidVelocity(72,Double.NEGATIVE_INFINITY));
		assertFalse (Ship.isValidVelocity(Double.NaN,54));
		assertFalse (Ship.isValidVelocity(20,Double.NaN));
	}
	
	@Test
	public final void setXVelocity_NegativeCase() {
		ship0_0_0_10_60_13.setXVelocity(-10);
		ship0_0_0_min10_45_15.setXVelocity(-10);
		assert Util.fuzzyEquals(-10, ship0_0_0_10_60_13.getXVelocity());
		assert Util.fuzzyEquals(-10, ship0_0_0_min10_45_15.getXVelocity());
	}
	
	@Test
	public final void setYVelocity_NegativeCase() {
		ship0_0_0_10_60_13.setYVelocity(-10);
		ship0_0_0_min10_45_15.setYVelocity(-50);
		assert Util.fuzzyEquals(-10, ship0_0_0_10_60_13.getYVelocity());
		assert Util.fuzzyEquals(-50, ship0_0_0_min10_45_15.getYVelocity());
	}
	
	@Test
	public final void isValidDirection_PositiveCase() {
		assertTrue(Ship.isValidDirection(7));
	}

	@Test 
	public final void isValidDirection_NegativeCase() {
		assertTrue(Ship.isValidDirection(-5));
	}
	
	@Test
	public final void isValidDirection_FalseCase() {
		assertFalse(Ship.isValidDirection(Double.NaN));
	}
	
	@Test
	public final void isValidRadius_TrueCase() {
		assertTrue(Ship.isValidRadius(26));
		assertTrue(Ship.isValidRadius(Double.POSITIVE_INFINITY));
	}

	@Test
	public final void isValidRadius_FalseCase() {
		assertFalse(Ship.isValidRadius(10));
		assertFalse(Ship.isValidRadius(4));
		assertFalse(Ship.isValidRadius(-9));
	}

	@Test
	public final void move_LegalCase() {
		ship0_0_0_10_60_13.move(5);
		assert Util.fuzzyEquals(0, ship0_0_0_10_60_13.getXPosition());
		assert Util.fuzzyEquals(50, ship0_0_0_10_60_13.getYPosition());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public final void move_IllegalCaseX() throws Exception {
		ship0_0_0_10_60_13.move(-1);
		assert Util.fuzzyEquals(0, ship0_0_0_10_60_13.getXPosition());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public final void move_IllegalCaseY() throws Exception {
		ship0_0_0_10_60_13.move(-1);
		assert Util.fuzzyEquals(0, ship0_0_0_10_60_13.getYPosition());
	}
	
	@Test
	public final void isValidDuration_TrueCase() {
		assertTrue (Ship.isValidDuration(0));
		assertTrue (Ship.isValidDuration(6333212));
	}
	
	@Test
	public final void isValidDuration_FalseCase() {
		assertFalse (Ship.isValidDuration(-4));
		assertFalse (Ship.isValidDuration(Double.NaN));
	}
	
	@Test
	public final void turn_PositiveAngle() {
		ship0_0_0_10_60_13.turn(Math.PI);
		assert Util.fuzzyEquals(4*Math.PI/3, ship0_0_0_10_60_13.getDirection());
	}
	
	@Test
	public final void turn_NegativeAngle() {
		ship0_34_0_min10_135_11.turn(-3*Math.PI/2);
		assert Util.fuzzyEquals(-3*Math.PI/4, ship0_34_0_min10_135_11.getDirection());
	}
	
	@Test
	public final void thrust_PositiveCase() {
		ship0_0_0_10_60_13.thrust(5);
		assert Util.fuzzyEquals(2.5, ship0_0_0_10_60_13.getXVelocity());
		assert Util.fuzzyEquals(10+5*Math.sin(Math.PI/3), ship0_0_0_10_60_13.getYVelocity());
	}
	
	@Test
	public final void thrust_NegativeCase() {
		ship0_0_0_min10_45_15.thrust(-10);
		assert Util.fuzzyEquals(0, ship0_0_0_min10_45_15.getXVelocity());
		assert Util.fuzzyEquals(-10, ship0_0_0_min10_45_15.getYVelocity());
	}
	
	@Test
	public final void getDistanceBetween_PositiveCase(){
		assert Util.fuzzyEquals(2, ship0_0_0_10_60_13.getDistanceBetween(ship0_30_0_min20_0_15));
	}
	
	@Test
	public final void getDistanceBetween_OuterZeroCase(){
		assert Util.fuzzyEquals(0, ship0_0_0_min10_45_15.getDistanceBetween(ship0_30_0_min20_0_15));
	}
	
	@Test
	public final void getDistanceBetween_SmallOverlap(){
		assert Util.fuzzyEquals(-6, ship0_0_0_10_60_13.getDistanceBetween(ship20_0_0_10_0_13));
	}
	
	@Test
	public final void getDistanceBetween_InnerZeroCase(){
		assert Util.fuzzyEquals(-22, ship0_34_0_min10_135_11.getDistanceBetween(ship0_30_0_min20_0_15));
	}
	
	@Test
	public final void getDistanceBetween_InsideCase(){
		assert Util.fuzzyEquals(-43, ship0_0_min15_0_157_50.getDistanceBetween(ship20_0_0_10_0_13));
	}
	
	@Test
	public final void getDistanceBetween_Concentric(){
		assert Util.fuzzyEquals(-28, ship0_0_0_min10_45_15.getDistanceBetween(ship0_0_0_10_60_13));   
		assert Util.fuzzyEquals(-28, ship0_0_0_10_60_13.getDistanceBetween(ship0_0_0_min10_45_15));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public final void getDistanceBetween_IllegalCase() throws Exception{
		ship0_0_0_min10_45_15.getDistanceBetween(null);
	}
	
	@Test
	public final void overlap_NormalCase(){
		assertFalse (ship0_0_0_10_60_13.overlap(ship0_30_0_min20_0_15));
	}
	
	@Test
	public final void overlap_NormalOverlapCase(){
		assertTrue (ship0_0_0_10_60_13.overlap(ship20_0_0_10_0_13));
		assertFalse (ship0_0_0_10_60_13.overlap(ship0_30_0_min20_0_15));
	}
	
	@Test
	public final void overlap_SelflOverlapCase(){
		assertTrue (ship0_0_0_10_60_13.overlap(ship0_0_0_10_60_13));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public final void overlap_IllegalCase() throws Exception{
		ship0_0_0_min10_45_15.overlap(null);
	}
	
	@Test
	public final void getTimeToCollision_CollisionCase(){
		assert Util.fuzzyEquals(2.2270900307699404, ship20_0_0_10_0_13.getTimeToCollision(ship0_30_10_0_108_15));
	}
	
	@Test
	public final void getTimeToCollision_OverlapCase(){
		assert Util.fuzzyEquals(Double.POSITIVE_INFINITY, ship0_30_0_min20_0_15.getTimeToCollision(ship0_34_0_min10_135_11)); 	//beginnen in overlap en gaan uit elkaar
		assert Util.fuzzyEquals(Double.POSITIVE_INFINITY, ship0_30_10_0_108_15.getTimeToCollision(ship0_34_0_min10_135_11));
	}
	
	@Test
	public final void getTimeToCollision_InitialContactCase() {
		assert Util.fuzzyEquals(Double.POSITIVE_INFINITY, ship0_0_0_min10_45_15.getTimeToCollision(ship0_30_0_min20_0_15));		//werkt niet ideaal, hoe zou het moeten?
	}
	
	@Test
	public final void getTimeToCollision_ParallelCase(){
		assert Util.fuzzyEquals(Double.POSITIVE_INFINITY, ship0_30_10_0_108_15.getTimeToCollision(ship40_min10_10_0_0_12));
		assert Util.fuzzyEquals(Double.POSITIVE_INFINITY, ship40_min10_10_0_0_12.getTimeToCollision(ship0_30_10_0_108_15));
	}
	
	@Test
	public final void getTimeToCollision_DivergingCase(){
		assert Util.fuzzyEquals(Double.POSITIVE_INFINITY, ship0_0_0_min10_45_15.getTimeToCollision(ship40_min10_10_0_0_12));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public final void getTimeToCollision_IllegalCase() throws Exception{
		ship0_0_0_min10_45_15.getTimeToCollision(null);
	}
	
	@Test
	public final void getCollisionPosition_CollisionCase() throws Exception{	
		double[] vector = ship20_0_0_10_0_13.getCollisionPosition(ship0_30_10_0_108_15);
		assert Util.fuzzyEquals(18.945454175390886, vector[0]);
		assert Util.fuzzyEquals(18.683018150140835, vector[1]);
	
	}
	
	@Test
	public final void getCollisionPosition_OverlapCase() throws Exception{			
		assertNull(ship0_30_10_0_108_15.getCollisionPosition(ship0_34_0_min10_135_11));	
		assertNull(ship0_30_0_min20_0_15.getCollisionPosition(ship0_34_0_min10_135_11));
		
	}
	
	@Test
	public final void getCollisionPosition_NoCollisionCase() throws Exception{			
		assertNull(ship40_min10_10_0_0_12.getCollisionPosition(ship0_0_0_min10_45_15));
		assertNull(ship0_30_10_0_108_15.getCollisionPosition(ship40_min10_10_0_0_12));
	}
	
	@Test
	public final void getCollisionPosition_InitialContactCase() throws Exception, Exception{			
		assertNull(ship0_0_0_min10_45_15.getCollisionPosition(ship0_30_0_min20_0_15));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public final void getCollisionPosition_IllegalCase() throws Exception{			
		ship0_0_0_min10_45_15.getCollisionPosition(null);
	}
}
